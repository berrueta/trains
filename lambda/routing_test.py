from unittest.mock import Mock
from routing import *
from circuit import simple_circuit

# Detection event


def test_detection_event_unknown_detector():
    circuit = Mock()
    trains_table = Mock()
    circuit.get_block_by_detector.return_value = None

    detection_event("NOT_A_DETECTOR_ID", circuit, trains_table)

    circuit.get_block_by_detector.assert_called_once_with("NOT_A_DETECTOR_ID")
    trains_table.assert_not_called()


def test_detection_event_no_train_warranted_there():
    circuit = Mock()
    trains_table = Mock()
    circuit.get_block_by_detector.return_value = "SOME_BLOCK"
    trains_table.get_train_by_block.return_value = None

    detection_event("DETECTOR_ID", circuit, trains_table)

    circuit.get_block_by_detector.assert_called_once_with("DETECTOR_ID")
    trains_table.get_train_by_block.assert_called_once_with("SOME_BLOCK")


def test_detection_event_train_already_there():
    circuit = Mock()
    trains_table = Mock()
    train = Mock(train_id="train_1", warrant=["SOME_BLOCK", "LATER_BLOCK"])
    circuit.get_block_by_detector.return_value = "SOME_BLOCK"
    trains_table.get_train_by_block.return_value = train

    detection_event("DETECTOR_ID", circuit, trains_table)

    circuit.get_block_by_detector.assert_called_once_with("DETECTOR_ID")
    trains_table.get_train_by_block.assert_called_once_with("SOME_BLOCK")
    train.save.assert_not_called()


def test_detection_event_train_making_progress():
    circuit = Mock()
    trains_table = Mock()
    train = Mock(train_id="train_1", warrant=["PREVIOUS_BLOCK", "SOME_BLOCK"], speed=1)
    circuit.get_block_by_detector.return_value = "SOME_BLOCK"
    trains_table.get_train_by_block.return_value = train

    detection_event("DETECTOR_ID", circuit, trains_table)

    circuit.get_block_by_detector.assert_called_once_with("DETECTOR_ID")
    trains_table.get_train_by_block.assert_called_once_with("SOME_BLOCK")
    assert train.warrant == ["SOME_BLOCK"]
    assert train.speed == 0
    train.save.assert_called_once()


# Calculate speed


def test_calculate_speed_train_has_no_further_warrant():
    assert calculate_speed(Mock(warrant=["1"])) == 0


def test_calculate_speed_train_has_short_warrant():
    assert calculate_speed(Mock(warrant=["1", "2"])) == TRAIN_SLOW_SPEED


def test_calculate_speed_train_has_long_warrant():
    assert calculate_speed(Mock(warrant=["1", "2", "3"])) == TRAIN_MAX_SPEED


# Replanning


def test_replanning_case_1():
    circuit = simple_circuit()
    train1 = Mock(train_id="train_1", warrant=["block_4"], destination="block_1")
    train2 = Mock(train_id="train_2", warrant=["block_2"], destination="block_1")
    trains_table = Mock()
    trains_table.get_all.return_value = [train1, train2]
    trains_table.block_reservation_map.side_effect = [
        {"block_4": "train_1", "block_2": "train_2"},  # first time
        {"block_4": "train_1", "block_1": "train_1", "block_2": "train_2"},  # second time
    ]
    switch1 = Mock(position="turn")
    switches_table = Mock()
    switches_table.get_switch.return_value = switch1

    replanning(circuit, trains_table, switches_table)

    assert train1.warrant == ["block_4", "block_1"]
    assert train2.warrant == ["block_2", "block_3"]
    assert train1.speed == TRAIN_SLOW_SPEED
    assert train2.speed == TRAIN_SLOW_SPEED
    train1.save.assert_called_once()
    train2.save.assert_called_once()
    switches_table.get_switch.assert_called_once_with("switch_1")
    assert switch1.position == "straight"
    switch1.save.assert_called_once()
