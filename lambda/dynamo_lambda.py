import json
import logging

from iot import IoT
from dynamo_tables import TrainsTable, SwitchesTable
from routing import replanning
from circuit import simple_circuit

logger = logging.getLogger()
logger.setLevel(logging.INFO)

iot = IoT()
circuit = simple_circuit()
trains_table = TrainsTable()
switches_table = SwitchesTable()


def trains_table_update_destination_handler(event, context):
    """Reacts to trains destination updates and recalculates the plan"""

    on_insert_or_modify(event,
                        TrainsTable.extract_row_image,
                        lambda row: row.destination,
                        lambda row: replanning(circuit, trains_table, switches_table))
    return {}


def trains_table_update_speed_handler(event, context):
    """Reacts to trains speed updates and sends MQTT commands to the trains"""

    on_insert_or_modify(event,
                        TrainsTable.extract_row_image,
                        lambda row: row.speed,
                        lambda row: iot.send_train_speed_command(row.train_id, row.bluetooth_id, row.speed))
    return {}


def switches_table_update_position_handler(event, context):
    """Reacts to switches position updates and sends MQTT commands to the switches"""

    on_insert_or_modify(event,
                        SwitchesTable.extract_row_image,
                        lambda row: row.position,
                        lambda row: iot.send_switch_position_command(row.switch_id, row.position))
    return {}


def on_insert_or_modify(event, row_extract_function, relevant_change_function, processing_function):
    records = event['Records']
    for record in records:
        event_name = record['eventName']
        if event_name in ('INSERT', 'MODIFY'):
            if 'OldImage' in record['dynamodb']:
                old_image = record['dynamodb']['OldImage']
                old_row = row_extract_function(old_image)
            else:
                old_row = None
            new_image = record['dynamodb']['NewImage']
            new_row = row_extract_function(new_image)
            if old_row is None or (relevant_change_function(old_row) != relevant_change_function(new_row)):
                processing_function(new_row)
            else:
                logger.info("Skipping irrelevant dynamodb change event: %s", json.dumps(record, indent=2))
        else:
            logger.info("Discarding event: %s", json.dumps(record, indent=2))
    return {}
