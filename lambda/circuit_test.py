import pytest
import networkx
from circuit import *


# Shortest path

def test_shortest_path_from_node_to_itself():
    assert simple_circuit().shortest_path("block_1", "block_1") == ["block_1"]


def test_shortest_path_with_multiple_steps():
    assert simple_circuit().shortest_path("block_1", "block_4") == ["block_1", "block_3", "block_4"]


def test_shortest_path_when_source_does_not_exist():
    with pytest.raises(networkx.exception.NodeNotFound):
        simple_circuit().shortest_path("NOT_A_VALID_NODE", "block_1")


def test_shortest_path_when_destination_does_not_exist():
    with pytest.raises(networkx.exception.NodeNotFound):
        simple_circuit().shortest_path("block_1", "NOT_A_VALID_NODE")


# Switch positions

def test_switch_positions_for_empty_path():
    assert simple_circuit().get_switch_positions([]) == []


def test_switch_positions_for_path_of_length_1():
    assert simple_circuit().get_switch_positions(["block_4"]) == []


def test_switch_positions_for_path_with_invalid_nodes():
    assert simple_circuit().get_switch_positions(["NOT_A_VALID_NODE", "ANOTHER_INVALID_NODE"]) is None


def test_switch_positions_for_non_empty_path():
    assert simple_circuit().get_switch_positions(["block_2", "block_3", "block_4", "block_1"]) == [
        {'switch_id': 'switch_1', 'switch_position': 'straight'}
    ]


def test_switch_positions_if_path_is_invalid():
    assert simple_circuit().get_switch_positions(["block_2", "block_4"]) is None


# Blocks

def test_blocks():
    assert simple_circuit().blocks() == ["block_1", "block_2", "block_3", "block_4"]


# Get block by detector


def test_get_block_by_detector_unknown_detector():
    assert simple_circuit().get_block_by_detector("NOT_A_DETECTOR_ID") is None


def test_get_block_by_detector_valid_detector():
    assert simple_circuit().get_block_by_detector("detector_10") == "block_3"
