import logging
from dynamo_tables import TrainsTable, SwitchesTable
from routing import detection_event, replanning
from circuit import simple_circuit

logger = logging.getLogger()
logger.setLevel(logging.INFO)

circuit = simple_circuit()
trains_table = TrainsTable()
switches_table = SwitchesTable()


def sensor_update_handler(event, context):
    """Reacts to MQTT messages from the sensors"""
    logger.info("Received sensor update MQTT event: %s", str(event))
    detector_id = str(event["detector_id"])
    detection_event(detector_id, circuit, trains_table)
    replanning(circuit, trains_table, switches_table)
