#!/usr/local/bin/python3.7

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from signal import pause
from gpiozero import Button
import asyncio
import logging
import json
import board
import busio
import digitalio
from adafruit_mcp230xx.mcp23017 import MCP23017

logging.basicConfig(level=logging.INFO)

thing_name = "8dd77a98b8"
host = "a3qriky0jaido7-ats.iot.ap-southeast-2.amazonaws.com"
client_id = "sensors"

i2c = busio.I2C(board.SCL, board.SDA)
mcp = MCP23017(i2c)

mcp_interrupt = Button("GPIO16", active_state=False, pull_up=None)
reset_button = Button("GPIO19")


def reset(mqtt_client):
    logging.info("Sending reset MQTT message")
    mqtt_client.publishAsync("trains/reset", payload=None, QoS=1)


def mcp_interrupt_handler(queue):
    flagged_pins = mcp.int_flag  # capture the flagged pins
    if len(flagged_pins) > 0:
        mcp.clear_ints()
    for pin in flagged_pins:
        detector_id = "detector_%d" % pin
        logging.info("Interrupt detected, enqueueing task for %s, queue size is %d", detector_id, queue.qsize())
        queue.put_nowait(detector_id)


async def send_detector_signal(mqtt_client, detector_id):
    logging.info("Sending sensor signal for sensor %s", detector_id)
    payload = {"detector_id": detector_id}
    mqtt_client.publishAsync("sensor/presence", payload=json.dumps(payload), QoS=1)


def mqtt_client_connect():
    logging.info("Connecting to AWS IoT...")

    client = AWSIoTMQTTClient(client_id)
    client.configureEndpoint(host, 8883)
    client.configureCredentials("certs/AmazonRootCA1.pem",
                                "certs/%s-private.pem.key" % thing_name,
                                "certs/%s-certificate.pem.crt" % thing_name)
    client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
    client.configureDrainingFrequency(2)  # Draining: 2 Hz
    client.configureConnectDisconnectTimeout(10)  # 10 sec
    client.configureMQTTOperationTimeout(5)  # 5 sec

    client.connect()
    logging.info("Connected to AWS IoT")

    return client


async def send_mqtt_message_task(task_name, queue, mqtt_client):
    logging.info("Task %s is running and picking messages from the queue...", task_name)
    while True:
        detector_id = await queue.get()
        logging.info("Task %s picks from the queue event about %s, there are still %d messages",
                     task_name, detector_id, queue.qsize())
        await send_detector_signal(mqtt_client, detector_id)
        queue.task_done()


async def init():
    # Configure all MCP pins as inputs
    mcp.interrupt_configuration = 0x0000  # trigger interrupt on value change
    mcp.interrupt_enable = 0xFFFF  # enable interrupts
    mcp.io_control = mcp.io_control | (1 << 6)  # internally connect the two interrupt pins
    for pin_number in range(0, 16):
        pin = mcp.get_pin(pin_number)
        pin.direction = digitalio.Direction.INPUT
        pin.pull = digitalio.Pull.UP
    mcp.clear_ints()

    mqtt_client = mqtt_client_connect()

    reset_button.when_pressed = lambda: reset(mqtt_client)
    # mcp_interrupt.when_activated = lambda: mcp_interrupt_handler(mqtt_client, queue)

    queue = asyncio.Queue(maxsize=16)

    sender_task = asyncio.create_task(send_mqtt_message_task("send_mqtt_message_task", queue, mqtt_client))
    polling_task = asyncio.create_task(polling_loop(queue))
    await asyncio.gather(sender_task, polling_task)


# Unfortunately I haven't figured how to make gpiozero's interrupt-driven callbacks play nicely with asyncio, so
# I had to implement polling
async def polling_loop(queue):
    logging.info("Entering polling loop...")
    while True:
        # logging.info("Looping...")
        mcp_interrupt_handler(queue)
        await asyncio.sleep(0.1)


if __name__ == "__main__":
    logging.info("Entering event loop...")
    asyncio.run(init())
