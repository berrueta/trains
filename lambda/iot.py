import boto3
import json
import logging

logger = logging.getLogger()


class IoT:
    def __init__(self):
        self.iot_data = boto3.client('iot-data')
        self.train_control_topic = 'trains/train-control'
        self.switch_control_topic = 'trains/switch-control'

    def send_train_speed_command(self, train_id, bluetooth_id, speed):
        """Send MQTT command to the train"""
        logger.info("Train %s with bluetooth id %s speed set to %d", train_id, bluetooth_id, speed)
        message_payload = {
            'train_id': train_id,
            'bluetooth_id': bluetooth_id,
            'speed': speed
        }
        self.iot_data.publish(
            topic=self.train_control_topic,
            qos=1,
            payload=json.dumps(message_payload)
        )

    def send_switch_position_command(self, switch_id, position):
        """Send MQTT command to the switch"""
        logger.info("Switch %s position set to %s", switch_id, position)
        message_payload = {
            'switch_id': switch_id,
            'position': position
        }
        self.iot_data.publish(
            topic=self.switch_control_topic,
            qos=1,
            payload=json.dumps(message_payload)
        )
