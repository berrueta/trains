from unittest import mock
from unittest.mock import Mock
from dynamo_tables import *


def test_trains_table_get_train_by_block_successfully():
    train1 = Mock(warrant=["SOME_BLOCK"])
    with mock.patch.object(TrainsTable, "get_all") as get_all_mock:
        get_all_mock.return_value = [train1]
        assert TrainsTable().get_train_by_block("SOME_BLOCK") == train1


def test_trains_table_get_train_by_block_when_no_train_in_that_block():
    with mock.patch.object(TrainsTable, "get_all") as get_all_mock:
        get_all_mock.return_value = []
        assert TrainsTable().get_train_by_block("EMPTY_BLOCK") is None
