import logging
from dynamo_tables import TrainsTable
from circuit import simple_circuit

logger = logging.getLogger()
logger.setLevel(logging.INFO)

circuit = simple_circuit()
trains_table = TrainsTable()


def reset(event, context):
    """Reacts to MQTT messages from the reset button"""
    logger.info("Received reset MQTT event")
    for train in trains_table.get_all():
        logger.info("Resetting train %s to destination %s", train.alias, train.destination)
        train.speed = 0
        train.warrant = [train.destination]
        train.save()
