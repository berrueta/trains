from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from bricknil import attach, start
from bricknil.hub import PoweredUpHub
from bricknil.sensor import TrainMotor
from curio import UniversalQueue
import logging
import json
from sys import platform
from Adafruit_PCA9685 import PCA9685

logging.basicConfig(level=logging.INFO)

thing_name = "4bd36a16e2"
train_control_topic = "trains/train-control"
switch_control_topic = "trains/switch-control"
host = "a3qriky0jaido7-ats.iot.ap-southeast-2.amazonaws.com"
client_id = "loco-control"
trains_by_uuid = {}
switches_by_id = {}


def on_mqtt_message(message):
    if message.topic in [train_control_topic, switch_control_topic]:
        payload = json.loads(message.payload)
        logging.info("Received MQTT message for topic %s: %s", message.topic, payload)
        if message.topic == train_control_topic:
            process_train_control_message(payload)
        elif message.topic == switch_control_topic:
            process_switch_control_message(payload)
    else:
        logging.error("Received message for unexpected topic %s", message.topic)


def process_train_control_message(payload):
    bluetooth_id = payload['bluetooth_id']
    if bluetooth_id in trains_by_uuid:
        train = trains_by_uuid[bluetooth_id]
        speed = payload['speed']
        logging.info("Setting speed of train %s to %d", train.name, speed)
        train.speed_command(speed)
    else:
        logging.info("Received message for unknown train %s", bluetooth_id)


def process_switch_control_message(payload):
    switch_id = payload['switch_id']
    if switch_id in switches_by_id:
        switch = switches_by_id[switch_id]
        position = payload['position']
        logging.info("Setting switch %s to position %s", switch.id, position)
        switch.set_position(position)
    else:
        logging.info("Received message for unknown switch %s", switch_id)


def connect_mqtt_client():
    logging.info("Connecting to AWS IoT...")

    client = AWSIoTMQTTClient(client_id)
    client.configureEndpoint(host, 8883)
    client.configureCredentials("certs/AmazonRootCA1.pem",
                                "certs/%s-private.pem.key" % thing_name,
                                "certs/%s-certificate.pem.crt" % thing_name)
    client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
    client.configureDrainingFrequency(2)  # Draining: 2 Hz
    client.configureConnectDisconnectTimeout(10)  # 10 sec
    client.configureMQTTOperationTimeout(5)  # 5 sec

    client.onMessage = on_mqtt_message

    client.connect()
    logging.info("Connected to AWS IoT")

    for topic in [train_control_topic, switch_control_topic]:
        logging.info("Listening to messages in topic %s", topic)
        client.subscribeAsync(topic, QoS=1)

    return client


class ServoSwitch:
    def __init__(self, id, servo):
        self.id = id
        self.servo = servo

    def set_position(self, position):
        if position == 'straight':
            self.servo.value = 0.0
        elif position == 'turn':
            self.servo.value = 0.9  # determined experimentally on a right switch
        else:
            logging.error("Unrecognised switch position '%s' for switch %s", position, self.id)


class PCA9685Switch:
    def __init__(self, id, pwm, channel):
        self.id = id
        self.pwm = pwm
        self.channel = channel

    def set_position(self, position):
        if position == 'straight':
            self.pwm.set_pwm(self.channel, 0, 380)  # determined experimentally on a right switch
        elif position == 'turn':
            self.pwm.set_pwm(self.channel, 0, 260)  # determined experimentally on a right switch
        else:
            logging.error("Unrecognised switch position '%s' for switch %s", position, self.id)


@attach(TrainMotor, name='motor')
class Train(PoweredUpHub):
    def __init__(self, name, uuid, mac_address, query_port_info=False):
        ble_id = uuid if platform == "darwin" else mac_address # MacOS uses BluetoothID, Linux uses Mac address
        super().__init__(name, query_port_info, ble_id)
        self.uuid = uuid
        self.mac_address = mac_address
        self.command_queue = UniversalQueue()

    def speed_command(self, speed):
        self.command_queue.put(speed)

    async def run(self):
        logging.info("Running event loop for train %s", self.name)
        while True:
            speed = await self.command_queue.get()
            logging.info("Train %s received command to set speed: %d", self.ble_id, speed)
            await self.motor.set_speed(speed)  # FIXME: can we ramp_speed?


async def system():
    passenger_train = Train('Passenger train', uuid="633b24fa441c4b6aa157aeba148e4a21", mac_address="90:84:2B:01:EA:DA")
    cargo_train = Train('Cargo train', uuid="6a6267862bb84277a93bfa390dcd3d83", mac_address="90:84:2B:07:B7:73")

    for train in [passenger_train, cargo_train]:
        logging.info("Registering train %s with id %s", train.name, train.ble_id)
        trains_by_uuid[train.uuid] = train


def init():
    pwm = PCA9685()
    pwm.set_pwm_freq(60)  # Hz
    for switch_number in range(0, 16):
        switch = PCA9685Switch("switch_%d" % switch_number, pwm, channel=switch_number)
        switches_by_id[switch.id] = switch

    connect_mqtt_client()


if __name__ == "__main__":
    init()
    start(system)
