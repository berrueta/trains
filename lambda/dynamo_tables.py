from pynamodb.models import Model
from pynamodb.attributes import ListAttribute, NumberAttribute, UnicodeAttribute
from collections import ChainMap
import logging

logger = logging.getLogger()

_AWS_REGION = 'ap-southeast-2'


class TrainModel(Model):
    class Meta:
        table_name = 'trains'
        region = _AWS_REGION
    train_id = UnicodeAttribute(hash_key=True)
    bluetooth_id = UnicodeAttribute()
    speed = NumberAttribute(default=0)
    destination = UnicodeAttribute(null=True)
    warrant = ListAttribute(default=[])
    alias = UnicodeAttribute(null=True)


class TrainsTable:
    def get_train(self, train_id):
        return TrainModel.get(train_id)

    def get_all(self):
        return list(TrainModel.scan())

    def get_train_by_block(self, block_id):
        return next((train for train in self.get_all() if block_id in train.warrant), None)

    def block_reservation_map(self):
        return ChainMap(*[{block: train.train_id for block in train.warrant} for train in TrainModel.scan()])

    @staticmethod
    def extract_row_image(image):
        return TrainModel(
            train_id=image['train_id']['S'],
            bluetooth_id=image['bluetooth_id']['S'],
            speed=int(image['speed']['N']) if 'speed' in image else 0,
            destination=image['destination']['S'],
            warrant=[item['S'] for item in image['warrant']['L']] if 'warrant' in image else [],
            alias=image['alias']['S'] if 'alias' in image else None
        )


class SwitchModel(Model):
    class Meta:
        table_name = 'switches'
        region = _AWS_REGION
    switch_id = UnicodeAttribute(hash_key=True)
    position = UnicodeAttribute()


class SwitchesTable:
    def get_switch(self, switch_id):
        return SwitchModel.get(switch_id)

    @staticmethod
    def extract_row_image(image):
        return SwitchModel(
            switch_id=image['switch_id']['S'],
            position=image['position']['S']
        )
