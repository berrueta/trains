import logging
from itertools import takewhile, chain

# From https://stackoverflow.com/questions/37703609/using-python-logging-with-aws-lambda/56579088#56579088
if len(logging.getLogger().handlers) > 0:
    # The Lambda environment pre-configures a handler logging to stderr. If a handler is already configured,
    # `.basicConfig` does not execute. Thus we set the level directly.
    logging.getLogger().setLevel(logging.INFO)
else:
    logging.basicConfig(level=logging.INFO)


def detection_event(detector_id, circuit, trains_table):
    logging.info("Detection event: %s", detector_id)
    block_id = circuit.get_block_by_detector(detector_id)
    if block_id is None:
        logging.warning("Received detection event for unknown detector_id %s", detector_id)
    else:
        logging.info("Detection in block: %s", block_id)
        train = trains_table.get_train_by_block(block_id)
        if train is None:
            logging.warning("No train should be in block %s", block_id)
        elif block_id == train.warrant[0]:
            logging.info("Train %s was already known to be at %s", train.alias, block_id)
        else:
            train.warrant = train.warrant[train.warrant.index(block_id):]
            train.speed = calculate_speed(train)
            logging.info("Train %s has advanced until %s, speed is %d and new warrant is %s", train.alias, block_id, train.speed, str(train.warrant))
            train.save()


TRAIN_MAX_SPEED = 75
TRAIN_SLOW_SPEED = 45


def calculate_speed(train):
    if len(train.warrant) <= 1:
        return 0
    elif len(train.warrant) == 2:
        return TRAIN_SLOW_SPEED
    else:
        return TRAIN_MAX_SPEED


def replanning(circuit, trains_table, switches_table):
    trains = trains_table.get_all()
    # First check the invariant that warrants are non-overlapping
    reserved_blocks = list(chain(*[train.warrant for train in trains]))
    if len(set(reserved_blocks)) < len(reserved_blocks):
        logging.error("Aborting because the trains currently have overlapping warrants. Needs to be repaired manually. Reserved blocks: %s", str(reserved_blocks))
    else:
        # Replan every train individually
        for train in trains:
            if len(train.warrant) == 0:
                logging.info("Skipping train %s because it has no warrant so I don't know where it is", train.alias)
            elif len(train.warrant) > 1:
                logging.info("Skipping train %s because it already has a warrant to go somewhere: %s", train.alias, str(train.warrant))
            elif train.destination is None:
                logging.info("Skipping train %s because it has no destination", train.alias)
            elif train.destination not in circuit.blocks():
                logging.warning("Skipping train %s because its destination %s is not a block in the circuit %s", train.alias, train.destination, str(circuit.blocks()))
            else:
                last_known_position = train.warrant[0]
                logging.info("Replanning train %s, currently at %s", train.alias, last_known_position)
                path = circuit.shortest_path(last_known_position, train.destination)
                block_map = trains_table.block_reservation_map()
                train.warrant = list(takewhile(lambda block: block_map.get(block, None) in [None, train.train_id], path))
                train.speed = calculate_speed(train)
                switch_changes = circuit.get_switch_positions(train.warrant)
                for switch_change in switch_changes:
                    switch = switches_table.get_switch(switch_change["switch_id"])
                    switch.position = switch_change["switch_position"]
                    switch.save()
                logging.info("Train %s has speed %d for warrant %s on path %s with switches %s", train.alias, train.speed, str(train.warrant), str(path), str(switch_changes))
                train.save()
