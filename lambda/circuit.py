import logging
import networkx as nx


logger = logging.getLogger()


class Circuit:
    def __init__(self, graph):
        self.graph = graph

    def shortest_path(self, from_block, to_block):
        return nx.shortest_path(self.graph, from_block, to_block)

    def get_switch_positions(self, path):
        if len(path) < 2:
            return []
        elif nx.is_simple_path(self.graph, path):
            return [self.graph[current][next] for current, next in zip(path, path[1:]) if self.graph[current][next]]
        else:
            return None

    def blocks(self):
        return list(self.graph.nodes())

    def get_block_by_detector(self, detector_id):
        matching_block_ids = (block_id for (block_id, attrs) in self.graph.nodes.data() if attrs.get("detector_id", None) == detector_id)
        return next(matching_block_ids, None)


def simple_circuit():
    # For the moment, the circuit is a directed graph (no two-way tracks!)
    graph = nx.DiGraph()

    graph.add_node("block_1", detector_id="detector_8")
    graph.add_node("block_2", detector_id="detector_9")
    graph.add_node("block_3", detector_id="detector_10")
    graph.add_node("block_4", detector_id="detector_11")

    graph.add_edges_from([
        ("block_1", "block_3"),
        ("block_2", "block_3"),
        ("block_3", "block_4"),
        ("block_4", "block_1", {'switch_id': 'switch_1', 'switch_position': 'straight'}),
        ("block_4", "block_2", {'switch_id': 'switch_1', 'switch_position': 'turn'})
    ])

    return Circuit(graph)
