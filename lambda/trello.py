import json
import logging
import re
from requests.structures import CaseInsensitiveDict
from dynamo_tables import TrainsTable

logger = logging.getLogger()
logger.setLevel(logging.INFO)

trains_table = TrainsTable()


def extract_bluetooth_id(description):
    search = re.search("(.*)Bluetooth ID: ([0-9a-fA-F]+)(.*)", description, re.IGNORECASE)
    if search:
        return search.group(2)
    else:
        return None


def process_webhook_payload(payload):
    logger.info("Payload %s", json.dumps(payload, indent=2))
    action = payload['action']
    action_data = action['data']
    if action['type'] == 'updateCard' and 'listAfter' in action_data and 'listBefore' in action_data:
        card_id = str(action_data['card']['id'])
        list_after = action_data['listAfter']['name']
        train = trains_table.get_train(card_id)
        train.destination = list_after
        train.save()
    elif action['type'] == 'updateCard' and 'desc' in action_data['card']:
        card_id = str(action_data['card']['id'])
        description = str(action_data['card']['desc'])
        bluetooth_id = extract_bluetooth_id(description)
        if bluetooth_id:
            train = trains_table.get_train(card_id)
            train.bluetooth_id = bluetooth_id
            train.save()
    else:
        logger.info("Ignoring action %s with data %s", str(action), str(action_data))


def webhook_handler(event, context):
    """Trello webhook handler"""

    operation = event['httpMethod']
    headers = CaseInsensitiveDict(event['headers'])

    try:
        if operation == "HEAD":
            return {'statusCode': 200}
        elif (operation == "POST") and ('Content-type' in headers) and (headers['Content-type'] == "application/json"):
            process_webhook_payload(json.loads(event['body']))
            return {'statusCode': 202}
        else:
            logger.warning("Unrecognised HTTP event: %s", json.dumps(event, indent=2))
            return {'statusCode': 400}
    except Exception as e:
        logger.exception("Unexpected error, cannot process event %s", json.dumps(event, indent=2))
        return {'statusCode': 500, 'body': str(e)}
